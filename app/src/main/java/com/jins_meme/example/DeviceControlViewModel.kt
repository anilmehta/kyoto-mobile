package com.jins_meme.example


import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jins_meme.device.JinsMemeDevice
import com.jins_meme.device.data.*
import com.jins_meme.example.util.toTimestampFormat


class DeviceControlViewModel : ViewModel() {

    val deviceInfo = MutableLiveData<JinsMemeDeviceInfo>()
    val summaryData = MutableLiveData<JinsMemeSummaryData>()
    val currentData = MutableLiveData<JinsMemeCurrentData>()

    val isConnected = MutableLiveData<Boolean>().apply { value = false }
    val gyroSensor = MutableLiveData<Boolean>().apply { value = false }

    val summaryDataAt = MediatorLiveData<String>().also { res ->
        res.addSource(summaryData) {
            res.value = summaryData.value?.let { toTimestampFormat(it.capturedAt) }
        }
    }
    val currentReceivedCounter = MutableLiveData<Long>().apply { value = 0 }
    val summaryReceivedCounter = MutableLiveData<Long>().apply { value = 0 }
    val device = MutableLiveData<JinsMemeDevice>()
    fun loadJinsMemeDevice(jinsMemeDevice: JinsMemeDevice) {
        device.value = jinsMemeDevice
        deviceInfo.value = jinsMemeDevice.jinsMemeDeviceInfo
        isConnected.value = jinsMemeDevice.isConnected
    }
    fun clearJinsMemeDevice() {
        deviceInfo.value = null
        summaryData.value = null
        currentData.value = null
        device.value = null
        isConnected.value = false
    }
    fun clearJinsMemeData() {
        isConnected.value = false
        summaryData.value = null
        currentData.value = null
    }

    fun updateDeviceInfo() {
        this.deviceInfo.postValue(device.value!!.jinsMemeDeviceInfo)
    }

    fun updateSummaryData(summaryData: JinsMemeSummaryData) {
        this.summaryData.postValue(summaryData)
        this.summaryReceivedCounter.postValue(this.summaryReceivedCounter.value!! + 1)
    }

    fun updateCurrentData(currentData: JinsMemeCurrentData) {
        this.currentData.postValue(currentData)
        this.currentReceivedCounter.postValue(this.currentReceivedCounter.value!! + 1)
    }
}
