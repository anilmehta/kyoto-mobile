package com.jins_meme.example


import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel


class ScanViewModel : ViewModel() {

    val nowScanning = MutableLiveData<Boolean>().apply { value = false }

    fun updateScanStatus(scanning: Boolean) {
        nowScanning.value = scanning
    }
}
