package com.jins_meme.example


import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.afollestad.materialdialogs.GravityEnum
import com.afollestad.materialdialogs.MaterialDialog
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.jins_meme.device.JinsMemeCentral
import com.jins_meme.device.JinsMemeDevice
import com.jins_meme.device.JinsMemeUnavailableException
import com.jins_meme.device.data.JinsMemeCurrentData
import com.jins_meme.device.data.JinsMemeSummaryData
import com.jins_meme.example.databinding.FragmentDeviceControlBinding
import com.jins_meme.example.util.toStringTimestamp
import com.jins_meme.example.util.toTimestampFormat
import com.jins_meme.example.util.toast
import org.json.JSONArray
import org.json.JSONObject
import permissions.dispatcher.NeedsPermission
import permissions.dispatcher.RuntimePermissions
import java.io.*


@RuntimePermissions
class DeviceControlFragment : Fragment(), JinsMemeDevice.ConnectionStatusListener, JinsMemeDevice.LatestDataListener, JinsMemeDevice.DeviceStatusListener {

    companion object {
        const val SCAN_REQUEST_CODE = 0x0A
        const val JINS_MEME_ADDRESS = "JINS_MEME_ADDRESS"
    }

    private val viewModel by lazy { ViewModelProvider(this).get(DeviceControlViewModel::class.java) }
    private val dialog by lazy { createLoadingDialog(activity!!) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        DataBindingUtil.inflate<FragmentDeviceControlBinding>(inflater, R.layout.fragment_device_control, container, false).also {
            it.lifecycleOwner = this
            it.handler = this
            it.viewModel = viewModel
        }.root

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SCAN_REQUEST_CODE) {
                data?.let {
                    clearJinsMemeListener()
                    viewModel.clearJinsMemeDevice()

                    it.extras?.getString(JINS_MEME_ADDRESS)?.let { address ->
                        JinsMemeCentral.getJinsMemeDevice(address)?.let { device ->
                            viewModel.loadJinsMemeDevice(device)
                            setJinsMemeListener()
                        }
                    }
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        onRequestPermissionsResult(requestCode, grantResults)
    }

    @Suppress("UNUSED_PARAMETER")
    fun searchMeme(view: View) {
        openScanWindowWithPermissionCheck()
    }

    @Suppress("UNUSED_PARAMETER")
    fun connect(view: View) {
        try {
            viewModel.device.value!!.connect()
            activity?.runOnUiThread { dialog.show() }
        } catch (e: JinsMemeUnavailableException) {
            toast(context!!, e.message!!)
        }
    }

    @Suppress("UNUSED_PARAMETER")
    fun disconnect(view: View) {
        try {
            viewModel.device.value!!.disconnect()
        } catch (e: JinsMemeUnavailableException) {
            toast(context!!, e.message!!)
        }
    }

    @Suppress("UNUSED_PARAMETER")
    fun gyroSensorOn(view: View) {
        viewModel.device.value!!.enableGyro()
    }

    @NeedsPermission(
            Manifest.permission.BLUETOOTH,
            Manifest.permission.BLUETOOTH_ADMIN,
            Manifest.permission.ACCESS_COARSE_LOCATION)
    fun openScanWindow() {
        val intent = Intent(context, ScanActivity::class.java)
        startActivityForResult(intent, SCAN_REQUEST_CODE)
    }

    private fun setJinsMemeListener() {
        viewModel.device.value?.let {
            it.addConnectionStatusListener(this)
            it.addLatestDataListener(this)
            it.addDeviceStatusListener(this)
        }
    }

    private fun clearJinsMemeListener() {
        viewModel.device.value?.let {
            it.removeConnectionStatusListener(this)
            it.removeLatestDataListener(this)
            it.removeDeviceStatusListener(this)
        }
    }

    override fun connectError() {
        activity?.runOnUiThread {
            dialog.hide()
            toast(context!!, getString(R.string.connect_failed))
        }
    }

    override fun connected() {
        activity?.runOnUiThread { dialog.hide() }

        viewModel.updateDeviceInfo()
        viewModel.isConnected.postValue(true)
    }

    override fun disconnected() {
        activity?.runOnUiThread { viewModel.clearJinsMemeData() }
    }

    override fun statusChanged(processing: Boolean, gyroEnabled: Boolean) {
        viewModel.gyroSensor.postValue(gyroEnabled)
    }

    override fun received(current: JinsMemeCurrentData) {
        viewModel.updateCurrentData(current)
        context?.let { saveCurrentData(it,Gson().toJson(current)) }

    }

    override fun received(summary: JinsMemeSummaryData) {
        Log.e("summary_data",summary.toString());
        viewModel.updateSummaryData(summary)
        activity?.runOnUiThread {
            context?.let { saveSummaryData(it,Gson().toJson(summary), toStringTimestamp(summary.capturedAt)) }
            toast(context!!, toTimestampFormat(summary.capturedAt)) }
    }

    @kotlin.jvm.Throws(IOException::class)
    fun saveSummaryData(context: Context, jsonString: String?,timestamp:String) {
//        val dirPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS)
//        val dirPath=context.getExternalFilesDir(null)
        val dirPath = File(context.getExternalFilesDir(null), "jins_device_data")
        Log.e("file_d",dirPath.toString())
        val dirName="jins_data_$timestamp"
        val imageFile = File(dirPath, "$dirName.json")
        try {
            if (!dirPath.isDirectory) {
                dirPath.mkdirs()
            }
           imageFile.createNewFile()
            Log.e("file_path",dirPath.toString())
//        val dirName="jins_data_$timestamp"
//        val jsonFile = File(rootFolder, "$dirName.json")
            Log.e("json_file_name",imageFile.toString())
            writeObject(imageFile,jsonString)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }
    fun readCurrentData(file:File):String
    {
        val fileReader = FileReader(file)
        val bufferedReader = BufferedReader(fileReader)
        val stringBuilder = StringBuilder()
        var line: String = bufferedReader.readLine()
        Log.e("line_str",line)
//        while (line != null&&line.isNotEmpty()) {
////            stringBuilder.append(line).append("\n")
//            line = bufferedReader.readLine()
//        }
        bufferedReader.close()
        return line
    }
    @kotlin.jvm.Throws(IOException::class)
    fun saveCurrentData(context: Context, jsonString: String?) {
//        val dirPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS)
//        val dirPath=context.getExternalFilesDir(null)
        val dirPath = File(context.getExternalFilesDir(null), "jins_device_data")
        Log.e("file_current_data",dirPath.toString())
        val imageFile = File(dirPath, "jins_current_data.json")
        try {
            if (!dirPath.isDirectory) {
                dirPath.mkdirs()
            }
            if(imageFile.isFile)
            {
                val json=readCurrentData(imageFile)
                val obj=JSONObject(json)
                val arr=obj.getJSONArray("current_data")
                arr.put(JSONObject(jsonString))
                Log.e("current_data_arr",arr.toString())
                writeObject(imageFile,obj.put("current_data",arr).toString())
                   Log.e("read_data", readCurrentData(imageFile))
                Log.e("image_file_found",imageFile.toString())
            }
            else
            {
                Log.e("image_file_not_found",imageFile.toString())
                imageFile.createNewFile()
                Log.e("file_path",dirPath.toString())
//        val dirName="jins_data_$timestamp"
//        val jsonFile = File(rootFolder, "$dirName.json")
                Log.e("json_file_name",imageFile.toString())
                writeObject(imageFile,JSONObject().put("current_data",JSONArray().put(JSONObject(jsonString))).toString())

            }

        } catch (e: Exception) {
            e.printStackTrace()
        }


//        val rootFolder: File = context.filesDir


        //or IOUtils.closeQuietly(writer);
    }
    private fun writeObject(file: File,jsonString: String?)
    {
        val writer = FileWriter(file)
        writer.write(jsonString)
        writer.close()
    }
    private fun createLoadingDialog(context: Context): MaterialDialog {
        val result = MaterialDialog
            .Builder(context)
            .contentGravity(GravityEnum.CENTER)
            .content("Connecting...")
            .contentColorRes(android.R.color.white)
            .widgetColor(ContextCompat.getColor(context, android.R.color.darker_gray))
            .progress(true, 0)
            .cancelable(false)
            .progressIndeterminateStyle(true)
            .widgetColorRes(android.R.color.white)
            .build()

        result.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        return result
    }

}
