package com.jins_meme.example

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.jins_meme.device.JinsMemeDevice

class ScanActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scan)

        supportFragmentManager.beginTransaction()
            .replace(R.id.scanActivityContent, ScanFragment())
            .commit()
    }

    fun finishWithSelectedDevice(jinsMemeDevice: JinsMemeDevice) {
        val returnIntent = Intent()
        returnIntent.putExtra(DeviceControlFragment.JINS_MEME_ADDRESS, jinsMemeDevice.bluetoothDevice.address)
        setResult(Activity.RESULT_OK, returnIntent)
        finish()
    }
}
