package com.jins_meme.example.util

import android.content.Context
import android.content.SharedPreferences

object PreferenceUtil {

    private const val STORE_MODE_PASSWORD = "STORE_MODE_PASSWORD"

    private fun getPreferences(context: Context): SharedPreferences {
        return context.getSharedPreferences("JINS_MEME_SAMPLE_CODE_PREF", Context.MODE_PRIVATE)
    }

    private fun getEditor(context: Context): SharedPreferences.Editor {
        return getPreferences(context).edit()
    }

    fun putPassword(context: Context, text: String) {
        val e = getEditor(context)
        e.putString(STORE_MODE_PASSWORD, text)
        e.commit()
    }

    fun getPassword(context: Context): String {
        val pref = getPreferences(context)
        return pref.getString(STORE_MODE_PASSWORD, "0x0000")!!
    }
}