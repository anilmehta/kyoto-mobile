package com.jins_meme.example.util

import android.content.Context
import android.view.View
import androidx.databinding.BindingAdapter
import androidx.databinding.InverseMethod
import java.text.SimpleDateFormat
import java.util.*

@BindingAdapter("isVisibleIf")
fun View.isVisibleIf(flag: Boolean) {
    visibility = if (flag) View.VISIBLE else View.GONE
}

@BindingAdapter("isGoneIf")
fun View.isGoneIf(flag: Boolean) {
    visibility = if (flag) View.GONE else View.VISIBLE
}

fun toTimestampFormat(time: Long, format: String = "yyyy/MM/dd HH:mm") : String {
    return SimpleDateFormat(format, Locale.getDefault()).apply { timeZone = TimeZone.getDefault() }.format(Date(time))
}
fun toStringTimestamp(time:Long,format: String="yyyyMMdd_HHmm"):String
{
    return SimpleDateFormat(format, Locale.getDefault()).apply { timeZone = TimeZone.getDefault() }.format(Date(time))
}
fun toast(context: Context, message: String, duration: Int = android.widget.Toast.LENGTH_SHORT) {
    android.widget.Toast.makeText(context, message, duration).show()
}


object SafeUnbox {
    @JvmStatic
    @InverseMethod("box")
    fun unbox(boxed: Boolean?): Boolean {
        return boxed ?: false
    }

    @JvmStatic
    fun box(unboxed: Boolean): Boolean? {
        return unboxed
    }
}