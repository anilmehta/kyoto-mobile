package com.jins_meme.example

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.jins_meme.example.databinding.ItemDeviceBinding
import com.jins_meme.device.JinsMemeDevice

class DeviceRecyclerViewAdapter(private val listener: DeviceAdapterListener?) : RecyclerView.Adapter<DeviceRecyclerViewAdapter.DeviceViewHolder>(){

    private var layoutInflater: LayoutInflater? = null
    private val deviceList: MutableList<JinsMemeDevice> = mutableListOf()
    private val rssiList: MutableList<Int> = mutableListOf()

    inner class DeviceViewHolder(val binding: ItemDeviceBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DeviceViewHolder {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.context)
        }
        val binding = DataBindingUtil.inflate<ItemDeviceBinding>(layoutInflater!!, R.layout.item_device, parent, false)
        return DeviceViewHolder(binding)
    }

    override fun onBindViewHolder(holder: DeviceViewHolder, position: Int) {
        holder.binding.deviceName = deviceList[holder.adapterPosition].bluetoothDevice.name
        holder.binding.rssi = rssiList[holder.adapterPosition].toString()
        holder.binding.connected = deviceList[holder.adapterPosition].isConnected


        holder.binding.deviceLayout.setOnClickListener {
            listener?.onDeviceSelected(deviceList[holder.adapterPosition])
        }
    }

    override fun getItemCount(): Int {
        return deviceList.size
    }

    fun addItem(device: JinsMemeDevice, rssi: Int?) {
        val index = deviceList.map { it.bluetoothDevice.address }.indexOf(device.bluetoothDevice.address)
        if (index == -1) {
            deviceList.add(device)
            rssiList.add(rssi ?: 0)
            notifyItemInserted(deviceList.size - 1)
        } else {
            rssi?.let { rssiList[index] = it }
            notifyItemChanged(index)
        }
    }

    interface DeviceAdapterListener {
        fun onDeviceSelected(device: JinsMemeDevice)
    }
}