package com.jins_meme.example


import androidx.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.jins_meme.example.databinding.FragmentScanBinding
import com.jins_meme.device.JinsMemeCentral
import com.jins_meme.device.JinsMemeDevice
import com.jins_meme.device.JinsMemeUnavailableException
import com.jins_meme.example.util.toast
import kotlinx.android.synthetic.main.fragment_scan.*

class ScanFragment : Fragment(), DeviceRecyclerViewAdapter.DeviceAdapterListener, JinsMemeCentral.ScanListener {

    private val viewModel by lazy { ViewModelProvider(this).get(ScanViewModel::class.java) }
    private val adapter by lazy { DeviceRecyclerViewAdapter(this) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        DataBindingUtil.inflate<FragmentScanBinding>(inflater, R.layout.fragment_scan, container, false).also {
            it.lifecycleOwner = this
            it.handler = this
            it.viewModel = viewModel
        }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        deviceList.adapter = adapter
        deviceList.layoutManager = GridLayoutManager(context, 1)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        if (JinsMemeCentral.isScanning) {
            try {
                JinsMemeCentral.stopScan()
            } catch (e: JinsMemeUnavailableException) {
                toast(context!!, e.message!!)
            }
        }
    }

    @Suppress("UNUSED_PARAMETER")
    fun startScan(view: View) {
        try {
            JinsMemeCentral.startScan(this)
        } catch (e: JinsMemeUnavailableException) {
            toast(context!!, e.message!!)
            return
        }
        viewModel.updateScanStatus(true)
    }

    @Suppress("UNUSED_PARAMETER")
    fun stopScan(view: View) {
        try {
            JinsMemeCentral.stopScan()
        } catch (e: JinsMemeUnavailableException) {
            toast(context!!, e.message!!)
            return
        }
        viewModel.updateScanStatus(false)
    }

    @Suppress("UNUSED_PARAMETER")
    fun close(view: View) {
        activity?.finish()
    }

    override fun found(jinsMemeDevice: JinsMemeDevice, rssi: Int?) {
        adapter.addItem(jinsMemeDevice, rssi)
    }

    override fun onDeviceSelected(device: JinsMemeDevice) {
        (activity as ScanActivity).finishWithSelectedDevice(device)
    }
}
